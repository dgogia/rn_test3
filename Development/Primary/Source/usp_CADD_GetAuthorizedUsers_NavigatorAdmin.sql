﻿--Use for Index page of Users

--EXEC dbo.usp_CADD_GetAuthorizedUsers_NavigatorAdmin

USE [Common]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.usp_CADD_GetAuthorizedUsers_NavigatorAdmin') AND type in (N'P', N'PC'))
	DROP PROCEDURE dbo.usp_CADD_GetAuthorizedUsers_NavigatorAdmin
GO

CREATE PROCEDURE dbo.usp_CADD_GetAuthorizedUsers_NavigatorAdmin
AS
BEGIN
	
	SELECT DISTINCT
		p.Last AS LastName
		, IsNull(p.Preferred, p.First) AS FirstName
		, c.UserId
		, d.Name AS Department
		, l.Location
		, ISNULL((select 'Yes' from CADDInternalUsers c2 where c.userid = c2.userid and c2.assettypeid = 1), 'No') as MAA
		, ISNULL((select 'Yes' from CADDInternalUsers c3 where c.userid = c3.userid and c3.assettypeid = 2), 'No') as NMAA
	FROM CADDInternalUsers c
	INNER JOIN CADD_AssetType a ON c.AssetTypeId = a.AssetTypeId
	INNER JOIN CAPeople.dbo.PeopleMain p ON p.CAID = c.UserId
	INNER JOIN CAPeople.dbo.PeopleDepartment d ON p.Department = d.Department
	INNER JOIN CAPeople.dbo.PeopleLocation l ON p.LocationId = l.LocationId
	
END
GO

GRANT EXEC ON usp_CADD_GetAuthorizedUsers_NavigatorAdmin TO FullAccess

GO