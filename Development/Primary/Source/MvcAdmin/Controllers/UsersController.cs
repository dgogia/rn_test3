﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcAdmin.Models;
using System.Diagnostics;
using System.Data.Objects.DataClasses;
using System.Text;
using System.IO;
using System.Reflection;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;

namespace MvcAdmin.Controllers
{ 
    public class UsersController : Controller
    {

        private User u = new User();
        private CommonEntities c = new CommonEntities();
        private CAPeopleEntities p = new CAPeopleEntities();
        private static MemoryStream memoryStream;
        
        //
        // GET: /Users/      
       
        //call u.GetUsers to get list of users to display to page
        public ViewResult Index(string Filter)
        {
            return View(new queryTypeAndResults() { results = u.GetUsers(Filter), queryParam = Filter });
        }

        //
        // POST: /Users/

        [HttpPost] 
        public ActionResult Index(string PermAll, int?[] Insert, int?[] Perm, int[] IDs, DateTime?[] datepicker, string Filter)
        {
            //Debug.WriteLine(Insert.Count());
            Dictionary<int, DateTime?> dp = new Dictionary<int, DateTime?>();
            int i = 0;            
            

            if (IDs != null && Insert != null)
            {
                List<AuthUser> temp = u.GetUsers(Filter);
                foreach (int x in IDs)
                {
                    //find user with ID = x
                    AuthUser result = temp.Find(
                        delegate(AuthUser au)
                        {
                            return au.ID == x;
                        }
                    );

                    if (result != null)
                    {
                        if (!dp.ContainsKey(result.ID))
                        {
                            if (result.Date_Agreement_Rec != null && result.Date_Agreement_Rec == datepicker[i])  //if DateAgreement not changed, add date from DB to the dictionary
                                dp.Add(x, result.Date_Agreement_Rec);
                            else  //if DateAgreement changed, add date from textbox to the dictionary
                                dp.Add(x, datepicker[i]);                                
                        }
                    }

                    i++;
                }
            }
            
            List<AuthUser> users = new List<AuthUser>();
            //permission all users *displayed on page*
            if (IDs!=null && PermAll != null)
            {
                List<AuthUser> temp = u.GetUsers("All Employees");
                List<int?> p;
                i = 0;
               
                if (Perm != null) p = Perm.ToList();
                else p = new List<int?>();

                //only those users displayed on page get access
                
                foreach (int x in IDs)
                {
                    //find user with ID = x
                    AuthUser result = temp.Find(
                        delegate(AuthUser au)
                        {
                            return au.ID == x;
                        }
                    );

                    if (result != null)
                    {
                        users.Add(result);

                        //if "Permission All" is checked and all users are selected (else unchecked and all user's permissions not selected should not create date picker dictionary and also keep Perm array to null)
                        //if (PermAll == "all") //Commented for NOW therefore cannot revoke permissions from users by unchecking "Permission All" checkbox
                        //{
                            if (!dp.ContainsKey(result.ID))
                            {
                                if (result.Date_Agreement_Rec != null && result.Date_Agreement_Rec == datepicker[i])  //if DateAgreement not changed, add date from DB to the dictionary
                                    dp.Add(x, result.Date_Agreement_Rec);
                                else   //if DateAgreement changed, add date from textbox to the dictionary 
                                    dp.Add(x, datepicker[i]);
                            }

                            if (Perm == null || !Perm.Contains(x)) p.Add(x);
                        //}
                    }

                    i++;
                }
                
                Perm = p.ToArray();

            }
            //only update those rows that were changed
            else if (Insert != null)
            {               
                foreach (AuthUser x in u.GetUsers("All Employees"))
                {
                    if (Insert.Contains(x.ID))
                    {
                        //those rows that were changed
                        users.Add(x);
                    }
                }
            }
                               
                       
            //to know what radio button to keep checked
            ViewBag.Fil = Filter;                     

            
            foreach(AuthUser x in users)
            {
                //Debug.WriteLine(users.Count());
                //if (users.Count() == 1) Debug.WriteLine(users.First().Perm + " " + users.First().LastName);

                if (Perm != null)
                {
                    if ((Perm!=null&&Perm.Contains(x.ID)))
                    {
                        x.Perm = true;
                        x.MAA_Access = true;

                        //For HF
                        //if user does not have HF access already, INSERT HF permission for the user in CADDInternalUsers
                        if ((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 1 select a).Count() == 0)
                        {
                            //insert new permission into CADDInternalUsers
                            CADDInternalUser cu = new CADDInternalUser();
                            cu.UserId = x.ID;
                            cu.AssetTypeId = 1;
                            cu.DateModified = DateTime.Now;
                            cu.ClientAdministrator = false;
                            cu.DateAgreementReceived = dp[x.ID];
                            cu.DateAccessTurnedOn = cu.DateModified;
                            c.CADDInternalUsers.AddObject(cu);
                            c.SaveChanges();

                        }                         
                        else    //if user already has HF access
                        {
                            //INSERT or UPDATE DateAgreementReceived column (user's HF record) even if it is null
                            //if ((dp.ContainsKey(x.ID)&&dp[x.ID] != null) && (from a in c.CADDInternalUsers where a.UserId == x.ID && a.DateAgreementReceived == null select a).Count() > 0)
                            if ((dp.ContainsKey(x.ID)) && (from a in c.CADDInternalUsers where a.UserId == x.ID select a).Count() > 0)
                            {
                                CADDInternalUser cu = (from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 1 select a).First();
                                cu.UserId = x.ID;
                                cu.AssetTypeId = 1;
                                cu.DateModified = DateTime.Now;
                                cu.ClientAdministrator = false;
                                cu.DateAgreementReceived = dp[x.ID];
                                cu.DateAccessTurnedOn = cu.DateModified;
                                //c.CADDInternalUsers.AddObject(cu);
                                c.SaveChanges();
                            }
                        }

                        x.NMAA_Access = true;

                        //For PI
                        //if user does not have PI access already, INSERT PI permission for the user in CADDInternalUsers
                        if ((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 2 select a).Count() == 0)
                        {
                            //insert new permission into CADDInternalUsers
                            CADDInternalUser cu = new CADDInternalUser();
                            cu.UserId = x.ID;
                            cu.AssetTypeId = 2;
                            cu.DateModified = DateTime.Now;
                            cu.ClientAdministrator = false;
                            cu.DateAgreementReceived = dp[x.ID];
                            cu.DateAccessTurnedOn = cu.DateModified;
                            c.CADDInternalUsers.AddObject(cu);
                            c.SaveChanges();
                        }
                        else   //if user already has PI access 
                        {
                            //INSERT or UPDATE DateAgreementReceived column (user's PI record) even if it is null
                            //if ((dp.ContainsKey(x.ID) && dp[x.ID] != null) && (from r in c.CADDInternalUsers where r.UserId == x.ID && r.DateAgreementReceived == null select r).Count() > 0)
                            if ((dp.ContainsKey(x.ID)) && (from r in c.CADDInternalUsers where r.UserId == x.ID select r).Count() > 0)
                            {
                                CADDInternalUser cu = (from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 2 select a).First();
                                cu.UserId = x.ID;
                                cu.AssetTypeId = 2;
                                cu.DateModified = DateTime.Now;
                                cu.ClientAdministrator = false;
                                cu.DateAgreementReceived = dp[x.ID];
                                cu.DateAccessTurnedOn = cu.DateModified;
                                //c.CADDInternalUsers.AddObject(cu);
                                c.SaveChanges();

                            }
                        }

                    }                    
                    else
                    {
                        x.Perm = false;
                        x.MAA_Access = false;
                       
                        //if user has HF access, REMOVE/DELETE it
                        if ((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 1 select a).Count() > 0)
                        {
                            //remove from CADDInternalUsers table
                            c.CADDInternalUsers.DeleteObject((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 1 select a).FirstOrDefault());
                            c.SaveChanges();
                        }

                        x.NMAA_Access = false;

                        //if user has PI access, REMOVE/DELETE it
                        if ((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 2 select a).Count() > 0)
                        {
                            //remove from CADDInternalUsers table
                            c.CADDInternalUsers.DeleteObject((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 2 select a).First());
                            c.SaveChanges();
                        }
                    }
                }
                else if (Perm == null && Insert != null)   //if no user has permissions
                {
                    x.Perm = false;
                    x.MAA_Access = false;

                    //if user has HF access, REMOVE/DELETE it
                    if ((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 1 select a).Count() > 0)
                    {
                        //remove from CADDInternalUsers table
                        c.CADDInternalUsers.DeleteObject((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 1 select a).FirstOrDefault());
                        c.SaveChanges();
                    }
                    x.NMAA_Access = false;

                    //if user has PI access, REMOVE/DELETE it
                    if ((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 2 select a).Count() > 0)
                    {
                        //remove from CADDInternalUsers table
                        c.CADDInternalUsers.DeleteObject((from a in c.CADDInternalUsers where a.UserId == x.ID && a.AssetTypeId == 2 select a).First());
                        c.SaveChanges();
                    }
                }
                
                //c.SaveChanges();
                
            }

            
            return Index(Filter);
        }

        [HttpPost]        
        public JsonResult Search(string searchString)
        {
            
            //List<AuthUser> result = new List<AuthUser>();
            //List<AuthUser> emps = u.GetUsers("All Employees");
            List<AuthUser> emps = u.GetUsers("Employee Names");

            List<AuthUser> result = emps.Where(x => x.FirstName.ToLower().Contains(searchString.ToLower().Trim()) ||
                                    x.LastName.ToLower().Contains(searchString.ToLower().Trim())).OrderBy(x => x.FirstName).ThenBy(x => x.LastName).ToList();
            
            /*
            foreach(AuthUser x in emps)
            {
                if (x.FirstName.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) == 0 || x.LastName.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) == 0)
                {                    
                    result.Add(x);
                }
            }
            */
            return this.Json(result);

        }

        [HttpGet]
        public FileContentResult download()
        {
            // todo:  use a unique file name

            // convert to byte array
            // use a different encoding if needed
            var encoding = new System.Text.ASCIIEncoding();
            //byte[] returnContent = encoding.GetBytes(@"C:\RN_Users.xls");

            byte[] returnContent = memoryStream.GetBuffer();
            memoryStream.Close();

            return File(returnContent, "application/x-ms-excel", "UserPermissions.xls");

        }

/*
        [HttpPost]
        public ActionResult ExportExcel(List<int> ids)
        //public void ExportExcel(string ids) //List<int> ids
        {            
            List<AuthUser> table = new List<AuthUser>();
            foreach (AuthUser x in u.GetUsers("All Employees"))
            {
                if (ids.Contains(x.ID)) table.Add(x);

            }

            // Getting the complete workbook...
            HSSFWorkbook workbook = new HSSFWorkbook();
            //HSSFWorkbook workbook = new HSSFWorkbook(fs, true);

            // Getting the worksheet by its name...
            HSSFSheet sheet = (NPOI.HSSF.UserModel.HSSFSheet)workbook.CreateSheet("RN Users");
                
                memoryStream = new MemoryStream();
                

            HSSFRow titleRow = (NPOI.HSSF.UserModel.HSSFRow)sheet.CreateRow(0);
            HSSFRow titleRow2 = (NPOI.HSSF.UserModel.HSSFRow)sheet.CreateRow(1);

            titleRow.CreateCell(1).SetCellValue("RN Users");
            titleRow2.CreateCell(1).SetCellValue("As currently displayed on Admin Page");
                

            HSSFRow headerRow = (NPOI.HSSF.UserModel.HSSFRow)sheet.CreateRow(3);

            headerRow.CreateCell(0).SetCellValue("Last Name");
            headerRow.CreateCell(1).SetCellValue("First Name");
            headerRow.CreateCell(2).SetCellValue("Department");
            headerRow.CreateCell(3).SetCellValue("Location");
            headerRow.CreateCell(4).SetCellValue("Permissioned Users");
            headerRow.CreateCell(5).SetCellValue("Date Permissioned");
            headerRow.CreateCell(6).SetCellValue("Date Agreement Received");

            int i = 4;
            foreach (AuthUser x in table)
            {
                HSSFRow row = (NPOI.HSSF.UserModel.HSSFRow)sheet.CreateRow(i++);
                row.CreateCell(0).SetCellValue(x.LastName);
                row.CreateCell(1).SetCellValue(x.FirstName);
                row.CreateCell(2).SetCellValue(x.Department);
                row.CreateCell(3).SetCellValue(x.Location);
                row.CreateCell(4).SetCellValue(x.Perm ? "Yes" : "No");
                row.CreateCell(5).SetCellValue(x.Date_Turned_On.ToString());
                row.CreateCell(6).SetCellValue(x.Date_Agreement_Rec.ToString());
            }
            CellStyle titlestyle = workbook.CreateCellStyle();

            CellStyle headerstyle = workbook.CreateCellStyle();
            HSSFPalette palette = workbook.GetCustomPalette();
                
                
            // creating a new Color
            palette.SetColorAtIndex(NPOI.HSSF.Util.HSSFColor.DARK_BLUE.index, (byte) 35, (byte) 52, (byte) 81);
            headerstyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.DARK_BLUE.index;
            headerstyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            headerstyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerstyle.Alignment = HorizontalAlignment.CENTER;
            headerstyle.Rotation = (short) 45;
                
            // font color
            Font titlefont = (NPOI.HSSF.UserModel.HSSFFont)workbook.CreateFont();
            titlefont.Boldweight = (short)FontBoldWeight.BOLD;
            titlestyle.VerticalAlignment = VerticalAlignment.CENTER;
            titlestyle.Alignment = HorizontalAlignment.CENTER;
            titlestyle.SetFont(titlefont);

            Font headerfont = workbook.CreateFont();
            headerfont.Color = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            headerfont.Boldweight = (short)FontBoldWeight.BOLD;
            headerstyle.SetFont(headerfont);

            titleRow.GetCell(1).CellStyle = titlestyle;
            titleRow2.GetCell(1).CellStyle = titlestyle;

            headerRow.GetCell(0).CellStyle = headerstyle;
            headerRow.GetCell(1).CellStyle = headerstyle;
            headerRow.GetCell(2).CellStyle = headerstyle;
            headerRow.GetCell(3).CellStyle = headerstyle;
            headerRow.GetCell(4).CellStyle = headerstyle;
            headerRow.GetCell(5).CellStyle = headerstyle;
            headerRow.GetCell(6).CellStyle = headerstyle;


            workbook.Write(memoryStream);

            System.IO.File.WriteAllBytes(@"C:\RN_Users.xls", memoryStream.ToArray());
            //System.IO.File.WriteAllBytes(@"RN_Users.xls", memoryStream.ToArray());


            Microsoft.Office.Interop.Excel.Application exl = new Microsoft.Office.Interop.Excel.Application();

            exl.Visible = true;

            // todo:  use a unique file name per request

            string workbookPath = @"C:\RN_Users.xls";
            //string workbookPath = @"RN_Users.xls";
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = exl.Workbooks.Open(workbookPath,
                0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "",
                true, false, 0, true, false, false);

            Microsoft.Office.Interop.Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            string currentSheet = "RN Users";
            Microsoft.Office.Interop.Excel.Worksheet excelWorksheet =
                (Microsoft.Office.Interop.Excel.Worksheet)excelSheets.get_Item(currentSheet);

            Microsoft.Office.Interop.Excel.Range rng =
                (Microsoft.Office.Interop.Excel.Range)excelWorksheet.get_Range("A1", "G2000");

            rng.EntireColumn.AutoFit();
            rng.EntireRow.AutoFit();
            return null;                                
           
        }
*/

        [HttpGet]
        public FileContentResult ExportExcelSubset(string query)
        {
            List<AuthUser> table = new List<AuthUser>();
            foreach (AuthUser x in u.GetUsers(query))
            {
                table.Add(x);
            }

            HSSFWorkbook workbook = new HSSFWorkbook();

            // Getting the worksheet by its name...
            HSSFSheet sheet = (NPOI.HSSF.UserModel.HSSFSheet)workbook.CreateSheet("RN Users");

            memoryStream = new MemoryStream();

            HSSFRow titleRow = (NPOI.HSSF.UserModel.HSSFRow)sheet.CreateRow(0);
            HSSFRow titleRow2 = (NPOI.HSSF.UserModel.HSSFRow)sheet.CreateRow(1);

            //When page first opens
            if (query == null) query = "All Employees";
            titleRow.CreateCell(1).SetCellValue("C|A Users");
            titleRow2.CreateCell(1).SetCellValue(query);
            //titleRow2.CreateCell(1).SetCellValue("As currently displayed on Admin Page");

            HSSFRow headerRow = (NPOI.HSSF.UserModel.HSSFRow)sheet.CreateRow(3);

            headerRow.CreateCell(0).SetCellValue("Last Name");
            headerRow.CreateCell(1).SetCellValue("First Name");
            headerRow.CreateCell(2).SetCellValue("Department");
            headerRow.CreateCell(3).SetCellValue("Location");
            headerRow.CreateCell(4).SetCellValue("Permissioned Users");
            headerRow.CreateCell(5).SetCellValue("Date Permissioned");
            headerRow.CreateCell(6).SetCellValue("Date Agreement Received");

            int i = 4;
            foreach (AuthUser x in table)
            {
                HSSFRow row = (NPOI.HSSF.UserModel.HSSFRow)sheet.CreateRow(i++);
                row.CreateCell(0).SetCellValue(x.LastName);
                row.CreateCell(1).SetCellValue(x.FirstName);
                row.CreateCell(2).SetCellValue(x.Department);
                row.CreateCell(3).SetCellValue(x.Location);
                row.CreateCell(4).SetCellValue(x.Perm ? "Yes" : "No");
                row.CreateCell(5).SetCellValue(x.Date_Turned_On.ToString());
                row.CreateCell(6).SetCellValue(x.Date_Agreement_Rec.ToString());
            }

            //set column width - Auto-size each column
            for (var j = 0; j < sheet.GetRow(3).LastCellNum; j++)
                sheet.AutoSizeColumn(j);

            //set header row height
            headerRow.Height = 2000;

            CellStyle titlestyle = workbook.CreateCellStyle();

            CellStyle headerstyle = workbook.CreateCellStyle();
            HSSFPalette palette = workbook.GetCustomPalette();

            // creating a new Color
            palette.SetColorAtIndex(NPOI.HSSF.Util.HSSFColor.DARK_BLUE.index, (byte)35, (byte)52, (byte)81);
            headerstyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.DARK_BLUE.index;
            headerstyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            headerstyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerstyle.Alignment = HorizontalAlignment.CENTER;
            headerstyle.Rotation = (short)45;

            // font color
            Font titlefont = (NPOI.HSSF.UserModel.HSSFFont)workbook.CreateFont();
            titlefont.Boldweight = (short)FontBoldWeight.BOLD;
            titlestyle.VerticalAlignment = VerticalAlignment.CENTER;
            titlestyle.Alignment = HorizontalAlignment.CENTER;
            titlestyle.SetFont(titlefont);

            Font headerfont = workbook.CreateFont();
            headerfont.Color = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            headerfont.Boldweight = (short)FontBoldWeight.BOLD;
            headerstyle.SetFont(headerfont);

            titleRow.GetCell(1).CellStyle = titlestyle;
            titleRow2.GetCell(1).CellStyle = titlestyle;

            headerRow.GetCell(0).CellStyle = headerstyle;
            headerRow.GetCell(1).CellStyle = headerstyle;
            headerRow.GetCell(2).CellStyle = headerstyle;
            headerRow.GetCell(3).CellStyle = headerstyle;
            headerRow.GetCell(4).CellStyle = headerstyle;
            headerRow.GetCell(5).CellStyle = headerstyle;
            headerRow.GetCell(6).CellStyle = headerstyle;
            
            workbook.Write(memoryStream);
            return File(memoryStream.ToArray(), "application/vnd.ms-excel", "RN_Users.xls");
        }
      

        protected override void Dispose(bool disposing)
        {
            c.Dispose();
            p.Dispose();
            base.Dispose(disposing);
        }
    }
}
