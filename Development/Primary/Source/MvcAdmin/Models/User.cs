﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq.Expressions;


namespace MvcAdmin.Models
{
    public class AuthUser
    {
        [Key]
        public int ID { get; set; }        
        public string FirstName { get; set; }        
        public string LastName { get; set; }        
        public string Department { get; set; }        
        public string Location { get; set; }
        public bool Perm { get; set; }
        public bool MAA_Access { get; set; }        
        public bool NMAA_Access { get; set; }        
        public DateTime? Date_Agreement_Rec { get; set; }        
        public DateTime? Date_Turned_On { get; set; }        
    }

    public class queryTypeAndResults
    {
        public IEnumerable<MvcAdmin.Models.AuthUser> results { get; set; }
        public string queryParam;
    }

    public class User
    {

        public List<AuthUser> GetUsers(string Filter)
        {            
            List<AuthUser> allemp = new List<AuthUser>();
            List<AuthUser> u = new List<AuthUser>();
            CommonEntities c = new CommonEntities();
            CAPeopleEntities p = new CAPeopleEntities();

            List<CADDInternalUser> pu = c.CADDInternalUsers.Distinct().ToList();
            List<PeopleMain> allu = p.PeopleMains.Distinct().ToList();
            List<PeopleDepartment> dept = p.PeopleDepartments.Distinct().ToList();
            List<PeopleLocation> loc = p.PeopleLocations.Distinct().ToList();            


            //when page first opens
            if (Filter == null) Filter = "All Employees";

            
            if (Filter.Equals("Permissioned Users"))
            {
                var authusers = from i in pu
                                join m in allu on i.UserId equals m.CAID
                                join d in dept on m.Department equals d.Department
                                join l in loc on m.LocationID equals l.LocationID
                                where m.StatusID == 1
                                select new
                                {
                                    ID = i.UserId,
                                    FirstName = m.Preferred == null ? m.First : m.Preferred,
                                    LastName = m.Last,
                                    Department = d.Name,
                                    Location = l.Location,
                                    MAA_Access = (from i2 in pu where i.UserId == i2.UserId && i2.AssetTypeId == 1 select i2).Count() == 0 ? false : true,
                                    NMAA_Access = (from i3 in pu where i.UserId == i3.UserId && i3.AssetTypeId == 2 select i3).Count() == 0 ? false : true,                                    
                                    Date_Agreement_Rec = (from i4 in pu where i.UserId == i4.UserId select i4.DateAgreementReceived).First(),
                                    Date_Turned_On = (from i5 in pu where i.UserId == i5.UserId select i5.DateAccessTurnedOn).First()
                                };

                
                    authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
                

                foreach (var x in authusers)
                {
                    AuthUser au = new AuthUser();
                    au.ID = x.ID;
                    au.FirstName = x.FirstName;
                    au.LastName = x.LastName;
                    au.Department = x.Department;
                    au.Location = x.Location;
                    //au.Perm = (x.MAA_Access && x.NMAA_Access);
                    au.Perm = (x.MAA_Access || x.NMAA_Access);  //permission to either HF or PI (PBI 6894)
                    au.MAA_Access = x.MAA_Access;
                    au.NMAA_Access = x.NMAA_Access;
                    au.Date_Agreement_Rec = x.Date_Agreement_Rec;
                    au.Date_Turned_On = x.Date_Turned_On;
                    u.Add(au);
                }
            }

            else if (Filter.Equals("Employee Names"))
            {
                List<AuthUser> allUsers = new List<AuthUser>();
                foreach (PeopleMain user in allu.Where(x => x.CAID != null && x.StatusID == 1))
                        allUsers.Add(new AuthUser { FirstName = user.Preferred, LastName = user.Last, ID = user.CAID.Value });
                return allUsers;
            }

            else if (Filter.Equals("All Employees"))
            {
                var authusers = from m in allu
                                join d in dept on m.Department equals d.Department
                                join l in loc on m.LocationID equals l.LocationID
                                where m.CAID != null && m.StatusID == 1

                                select new
                                {
                                    ID = m.CAID,
                                    FirstName = m.Preferred == null ? m.First : m.Preferred,
                                    LastName = m.Last,
                                    Department = d.Name,
                                    Location = l.Location,
                                    MAA_Access = (from i2 in pu where m.CAID == i2.UserId && i2.AssetTypeId == 1 select i2).Count() == 0 ? false : true,
                                    NMAA_Access = (from i3 in pu where m.CAID == i3.UserId && i3.AssetTypeId == 2 select i3).Count() == 0 ? false : true,
                                    Date_Agreement_Rec = (from i4 in pu where m.CAID == i4.UserId select i4).Count() == 0 ? (DateTime?)null : (from i4 in pu where m.CAID == i4.UserId select i4.DateAgreementReceived).First(),
                                    Date_Turned_On = (from i5 in pu where m.CAID == i5.UserId select i5).Count() == 0 ? (DateTime?)null : (from i5 in pu where m.CAID == i5.UserId select i5.DateAccessTurnedOn).First()
                                };

                //var duplicates = from imr in authusers
                //                 group imr by imr.ID into g
                //                 where g.Count() > 1
                //                 select g.First();

                //var t1 = authusers.Where(i => i.ID == 78500).ToList();

                authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);

                foreach (var x in authusers)
                {
                    AuthUser au = new AuthUser();
                    au.ID = (int)x.ID;
                    au.FirstName = x.FirstName;
                    au.LastName = x.LastName;
                    au.Department = x.Department;
                    au.Location = x.Location;
                    //au.Perm = (x.MAA_Access && x.NMAA_Access);
                    au.Perm = (x.MAA_Access || x.NMAA_Access);  //permission to either HF or PI (PBI 6894)
                    au.MAA_Access = x.MAA_Access;
                    au.NMAA_Access = x.NMAA_Access;
                    au.Date_Agreement_Rec = x.Date_Agreement_Rec;
                    au.Date_Turned_On = x.Date_Turned_On;
                    u.Add(au);

                }
            }

            else if (Filter.Equals("Not Permissioned Users"))
            {
                var authusers = from m in allu
                                join d in dept on m.Department equals d.Department
                                join l in loc on m.LocationID equals l.LocationID
                                where m.CAID != null
                                && (from i2 in pu where m.CAID == i2.UserId && i2.AssetTypeId == 1 select i2).Count() == 0
                                && (from i3 in pu where m.CAID == i3.UserId && i3.AssetTypeId == 2 select i3).Count() == 0
                                && m.StatusID == 1
                                select new
                                {
                                    ID = m.CAID,
                                    FirstName = m.Preferred == null ? m.First : m.Preferred,
                                    LastName = m.Last,
                                    Department = d.Name,
                                    Location = l.Location,
                                    MAA_Access = false,
                                    NMAA_Access = false,
                                    Date_Agreement_Rec = (DateTime?)null,
                                    Date_Turned_On = (DateTime?)null
                                };

                authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);

                foreach (var x in authusers)
                {
                    AuthUser au = new AuthUser();
                    au.ID = (int)x.ID;
                    au.FirstName = x.FirstName;
                    au.LastName = x.LastName;
                    au.Department = x.Department;
                    au.Location = x.Location;
                    au.Perm = (x.MAA_Access || x.NMAA_Access);  //No permission to either HF or PI (PBI 6894)
                    au.MAA_Access = x.MAA_Access;
                    au.NMAA_Access = x.NMAA_Access;
                    au.Date_Agreement_Rec = x.Date_Agreement_Rec;
                    au.Date_Turned_On = x.Date_Turned_On;
                    u.Add(au);
                }
            }
    
            return u.ToList();
        }
        
   
    }

}
