﻿
--returns those CA employees who don't have HF or PI access at all (and not listed in dbo.CADDInternalUsers)

USE [Common]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.usp_CADD_GetInternalUsers_NavigatorAdmin') AND type in (N'P', N'PC'))
	DROP PROCEDURE dbo.usp_CADD_GetInternalUsers_NavigatorAdmin
GO

CREATE PROCEDURE dbo.usp_CADD_GetInternalUsers_NavigatorAdmin
AS
BEGIN

	SELECT	
		CAID AS UserId
		, 'I' AS UserTypeId
		, [Last] AS LastName
		, IsNull(Preferred, [First]) AS FirstName
	
	FROM capeople.dbo.PeopleMain 
	WHERE StatusID = 1
		AND CAID NOT IN (	SELECT UserId FROM dbo.CADDInternalUsers )
	ORDER BY [Last] + ', ' + IsNull(Preferred, [First])

END
GO

GRANT EXEC ON usp_CADD_GetInternalUsers_NavigatorAdmin TO FullAccess
GO