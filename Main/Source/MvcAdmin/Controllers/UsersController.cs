﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcAdmin.Models;
using System.Diagnostics;
using System.Data.Objects.DataClasses;

namespace MvcAdmin.Controllers
{ 
    public class UsersController : Controller
    {

        private User u = new User();
        private CommonEntities c = new CommonEntities();
        private CAPeopleEntities p = new CAPeopleEntities();

        //
        // GET: /Users/

       
       
        //call u.GetUsers to get list of users to display to page
        public ViewResult Index(string Filter, string sortBy)
        {
            
            return View(u.GetUsers(Filter, sortBy));
        }

        //
        // POST: /Users/

        [HttpPost] //get IDs of "checked" checkboxes and value of filter
        public ActionResult Index(int?[] MAA_Access, int?[] NMAA_Access, string Filter, string sortBy)
        {
            //to know what radio button to keep checked
            ViewBag.Fil = Filter;
            
            foreach(AuthUser x in u.GetUsers("All Users", sortBy))
            {

                if (MAA_Access != null)
                {
                    if (MAA_Access.Contains(x.ID))
                    {

                        x.MAA_Access = true;
                        //if user does not have HF access already

                        if (c.CADDInternalUsers.Where(a => a.UserId == x.ID && a.AssetTypeId == 1).Count() == 0)
                        {
                            //insert new permission into CADDInternalUsers
                            CADDInternalUser cu = new CADDInternalUser();
                            cu.UserId = x.ID;
                            cu.AssetTypeId = 1;
                            cu.DateModified = DateTime.Now;
                            cu.ClientAdministrator = false;
                            c.CADDInternalUsers.AddObject(cu);
                        }

                    }
                    else
                    {

                        x.MAA_Access = false;


                        //if user has HF access
                        if (c.CADDInternalUsers.Where(a => a.UserId == x.ID && a.AssetTypeId == 1).Count() > 0)
                        {
                            //remove from CADDInternalUsers table
                            c.CADDInternalUsers.DeleteObject(c.CADDInternalUsers.Where(a => a.UserId == x.ID && a.AssetTypeId == 1).First());

                        }

                    }
                }
                if (NMAA_Access != null)
                {
                    if (NMAA_Access.Contains(x.ID))
                    {

                        x.NMAA_Access = true;
                        //if user does not have PI access already

                        if (c.CADDInternalUsers.Where(a => a.UserId == x.ID && a.AssetTypeId == 2).Count() == 0)
                        {
                            //insert new permission into CADDInternalUsers
                            CADDInternalUser cu = new CADDInternalUser();
                            cu.UserId = x.ID;
                            cu.AssetTypeId = 2;
                            cu.DateModified = DateTime.Now;
                            cu.ClientAdministrator = false;
                            c.CADDInternalUsers.AddObject(cu);
                        }

                    }
                    else
                    {

                        x.NMAA_Access = false;


                        //if user has PI access
                        if (c.CADDInternalUsers.Where(a => a.UserId == x.ID && a.AssetTypeId == 2).Count() > 0)
                        {
                            //remove from CADDInternalUsers table
                            c.CADDInternalUsers.DeleteObject(c.CADDInternalUsers.Where(a => a.UserId == x.ID && a.AssetTypeId == 2).First());

                        }

                    }
                }
                c.SaveChanges();
            }
            
           
            return Index(Filter,sortBy);
        }


       
        
    
        //
        // GET: /Users/Details/5

        public ViewResult Details(int id)
        {
            AuthUser user = u.GetUsers("All Users",null).Where(x => x.ID == id).First();
            return View(user);
        }

        
        
        //
        // GET: /Users/Edit/5
 
        public ActionResult Edit(int id)
        {
            AuthUser user = u.GetUsers("All Users", null).Where(x => x.ID == id).First();
            return View(user);
        }

        //
        // POST: /Users/Edit/5

        [HttpPost]
        public ActionResult Edit(AuthUser user)
        {
            if (ModelState.IsValid)
            {
                
                if (user.MAA_Access && c.CADDInternalUsers.Where(a => a.UserId == user.ID && a.AssetTypeId ==1).Count() == 0)
                {
                    CADDInternalUser cu = new CADDInternalUser();
                    cu.UserId = user.ID;
                    cu.AssetTypeId = 1;
                    cu.DateModified = DateTime.Now;
                    cu.ClientAdministrator = false;
                    c.CADDInternalUsers.AddObject(cu);
                }
                else if (!user.MAA_Access && c.CADDInternalUsers.Where(a => a.UserId == user.ID && a.AssetTypeId == 1).Count() > 0)
                {
                    //remove from CADDInternalUsers table
                    c.CADDInternalUsers.DeleteObject(c.CADDInternalUsers.Where(a => a.UserId == user.ID && a.AssetTypeId == 1).First());
                }
                if (user.NMAA_Access && c.CADDInternalUsers.Where(a => a.UserId == user.ID && a.AssetTypeId == 2).Count() == 0)
                {
                    CADDInternalUser cu = new CADDInternalUser();
                    cu.UserId = user.ID;
                    cu.AssetTypeId = 2;
                    cu.DateModified = DateTime.Now;
                    cu.ClientAdministrator = false;
                    c.CADDInternalUsers.AddObject(cu);
                }
                else if (!user.NMAA_Access && c.CADDInternalUsers.Where(a => a.UserId == user.ID && a.AssetTypeId == 2).Count() > 0)
                {
                    //remove from CADDInternalUsers table
                    c.CADDInternalUsers.DeleteObject(c.CADDInternalUsers.Where(a => a.UserId == user.ID && a.AssetTypeId == 2).First());
                }
               
                c.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

       

        protected override void Dispose(bool disposing)
        {
            c.Dispose();
            p.Dispose();
            base.Dispose(disposing);
        }
    }
}