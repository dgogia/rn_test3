﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq.Expressions;

namespace MvcAdmin.Models
{
    public class AuthUser
    {
        [Key]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public bool MAA_Access { get; set; }
        public bool NMAA_Access { get; set; }

        
    }
    public class User
    {

        
        
        public List<AuthUser> GetUsers(string Filter, string sortBy)
        {

            List<AuthUser> u = new List<AuthUser>();
            CommonEntities c = new CommonEntities();
            CAPeopleEntities p = new CAPeopleEntities();

            List<CADDInternalUser> pu = c.CADDInternalUsers.Distinct().ToList();
            List<PeopleMain> allu = p.PeopleMains.Distinct().ToList();
            List<PeopleDepartment> dept = p.PeopleDepartments.Distinct().ToList();
            List<PeopleLocation> loc = p.PeopleLocations.Distinct().ToList();
            
            


            //when page first opens
            if (Filter == null) Filter = "All Users";

            
            if (Filter.Equals("Permissioned Users"))
            {
                var authusers = from i in pu
                                join m in allu on i.UserId equals m.CAID
                                join d in dept on m.Department equals d.Department
                                join l in loc on m.LocationID equals l.LocationID
                                select new
                                {
                                    ID = i.UserId,
                                    FirstName = m.Preferred == null ? m.First : m.Preferred,
                                    LastName = m.Last,
                                    Department = d.Name,
                                    Location = l.Location,
                                    MAA_Access = (from i2 in pu where i.UserId == i2.UserId && i2.AssetTypeId == 1 select i2).Count() == 0 ? false : true,
                                    NMAA_Access = (from i3 in pu where i.UserId == i3.UserId && i3.AssetTypeId == 2 select i3).Count() == 0 ? false : true
                                };

                if (sortBy == null)
                {
                    authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
                }
                else
                {
                    if (sortBy.Equals("Last Name")) authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("First Name")) authusers = authusers.Distinct().OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
                    if (sortBy.Equals("Department")) authusers = authusers.Distinct().OrderBy(x => x.Department).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Location")) authusers = authusers.Distinct().OrderBy(x => x.Location).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Hedge Fund")) authusers = authusers.Distinct().OrderByDescending(x => x.MAA_Access).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Private Investments")) authusers = authusers.Distinct().OrderByDescending(x => x.NMAA_Access).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                }
                foreach (var x in authusers)
                {
                    AuthUser au = new AuthUser();
                    au.ID = x.ID;
                    au.FirstName = x.FirstName;
                    au.LastName = x.LastName;
                    au.Department = x.Department;
                    au.Location = x.Location;
                    au.MAA_Access = x.MAA_Access;
                    au.NMAA_Access = x.NMAA_Access;
                    u.Add(au);
                }
            }
            else if (Filter.Equals("All Users"))
            {
                var authusers = from m in allu
                                join d in dept on m.Department equals d.Department
                                join l in loc on m.LocationID equals l.LocationID
                                where m.CAID != null
                                select new
                                {
                                    ID = m.CAID,
                                    FirstName = m.Preferred == null ? m.First : m.Preferred,
                                    LastName = m.Last,
                                    Department = d.Name,
                                    Location = l.Location,
                                    MAA_Access = (from i2 in pu where m.CAID == i2.UserId && i2.AssetTypeId == 1 select i2).Count() == 0 ? false : true,
                                    NMAA_Access = (from i3 in pu where m.CAID == i3.UserId && i3.AssetTypeId == 2 select i3).Count() == 0 ? false : true
                                };

                if (sortBy == null)
                {
                    authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
                }
                else
                {
                    if (sortBy.Equals("Last Name")) authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("First Name")) authusers = authusers.Distinct().OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
                    if (sortBy.Equals("Department")) authusers = authusers.Distinct().OrderBy(x => x.Department).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Location")) authusers = authusers.Distinct().OrderBy(x => x.Location).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Hedge Fund")) authusers = authusers.Distinct().OrderByDescending(x => x.MAA_Access).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Private Investments")) authusers = authusers.Distinct().OrderByDescending(x => x.NMAA_Access).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                }
                
                foreach (var x in authusers)
                {
                    AuthUser au = new AuthUser();
                    au.ID = (int)x.ID;
                    au.FirstName = x.FirstName;
                    au.LastName = x.LastName;
                    au.Department = x.Department;
                    au.Location = x.Location;
                    au.MAA_Access = x.MAA_Access;
                    au.NMAA_Access = x.NMAA_Access;
                    u.Add(au);
                }
            }
            else if (Filter.Equals("Not Permissioned Users"))
            {
                var authusers = from m in allu
                                join d in dept on m.Department equals d.Department
                                join l in loc on m.LocationID equals l.LocationID
                                where m.CAID != null
                                && (from i2 in pu where m.CAID == i2.UserId && i2.AssetTypeId == 1 select i2).Count() == 0
                                && (from i3 in pu where m.CAID == i3.UserId && i3.AssetTypeId == 2 select i3).Count() == 0
                                select new
                                {
                                    ID = m.CAID,
                                    FirstName = m.Preferred == null ? m.First : m.Preferred,
                                    LastName = m.Last,
                                    Department = d.Name,
                                    Location = l.Location,
                                    MAA_Access =  false,
                                    NMAA_Access = false,
                                };
                if (sortBy == null)
                {
                    authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
                }
                else
                {
                    if (sortBy.Equals("Last Name")) authusers = authusers.Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("First Name")) authusers = authusers.Distinct().OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
                    if (sortBy.Equals("Department")) authusers = authusers.Distinct().OrderBy(x => x.Department).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Location")) authusers = authusers.Distinct().OrderBy(x => x.Location).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Hedge Fund")) authusers = authusers.Distinct().OrderByDescending(x => x.MAA_Access).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);
                    if (sortBy.Equals("Private Investments")) authusers = authusers.Distinct().OrderByDescending(x => x.NMAA_Access).ThenBy(x => x.LastName).ThenBy(x => x.FirstName);

                }
                foreach (var x in authusers)
                {
                    AuthUser au = new AuthUser();
                    au.ID = (int)x.ID;
                    au.FirstName = x.FirstName;
                    au.LastName = x.LastName;
                    au.Department = x.Department;
                    au.Location = x.Location;
                    au.MAA_Access = x.MAA_Access;
                    au.NMAA_Access = x.NMAA_Access;
                    u.Add(au);
                }
            }
            return u.ToList();
            }
        

   
        }

    }
